use rand::seq::SliceRandom;
use std::{
    fmt::{self, Display},
    ops::Range,
    str::FromStr,
};

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub struct Taquin {
    size: u8,
    map: Vec<u8>,
}

mod pos;
use pos::Pos;

impl Taquin {
    pub fn new(map: Vec<u8>) -> Option<Self> {
        let size = map.len();
        let size = (size as f64).sqrt() as u8;

        let tot: usize = (size * size).into();
        if tot != map.len() {
            return None;
        }

        let mut check = map.clone();
        check.sort();
        if check.iter().enumerate().any(|(id, &el)| id != el.into()) {
            return None;
        }

        Some(Taquin { size, map })
    }

    pub fn random(size: u8) -> Self {
        let mut random: Vec<u8> = (0..(size * size)).collect();
        random.shuffle(&mut rand::thread_rng());

        Taquin::new(random).unwrap()
    }

    pub fn is_solved(&self) -> bool {
        self.map
            .iter()
            .take(self.nb_on_board().last().unwrap().into())
            .enumerate()
            .all(|(id, &val)| id + 1 == val.into())
    }

    pub fn action(&mut self, action: u8) -> i8 {
        if !self.actions().contains(&action) {
            return -20;
        }

        let action = Pos::new(action, self.size);

        if self.map[action.val()] == 0 {
            return -10;
        }

        let pos = self
            .map
            .iter()
            .position(|&e| e == 0)
            .expect("Array should contain an empty cell");

        let empty = Pos::new(pos, self.size);

        let path = empty.position_to(action);

        if let Some(path) = path {
            let path: Vec<_> = path.collect();
            for pos in path.windows(2) {
                let p = &pos[0];
                let next = &pos[1];

                self.map.swap(next.val(), p.val());
            }

            -1
        } else {
            -5
        }
    }

    pub fn actions(&self) -> Range<u8> {
        0..(self.size * self.size)
    }

    pub fn nb_on_board(&self) -> Range<u8> {
        1..(self.size * self.size)
    }

    fn position(&self, val: u8) -> Pos {
        assert!(self.nb_on_board().contains(&val));
        Pos::new(self.map.iter().position(|&e| e == val).unwrap(), self.size)
    }

    pub fn dist_to_solved(&self) -> usize {
        self.nb_on_board()
            .map(|el| {
                let pos = self.position(el);

                pos.dist(&Pos::new(el - 1, self.size))
            })
            .sum()
    }
}

impl Display for Taquin {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "+")?;
        for _ in 0..self.size {
            write!(f, "--+")?;
        }
        write!(f, "\t+")?;
        for _ in 0..self.size {
            write!(f, "-+")?;
        }
        write!(f, "\n")?;

        for line in self.map.chunks(self.size.into()) {
            write!(f, "|")?;
            for &row in line {
                if row == 0 {
                    write!(f, "  ")?;
                } else {
                    write!(f, "{:>2}", row)?;
                }
                write!(f, "|")?;
            }
            write!(f, "\t|")?;
            for &row in line {
                if row == 0 {
                    write!(f, "X")?;
                } else {
                    write!(f, " ")?;
                }
                write!(f, "|")?;
            }
            write!(f, "\n")?;

            write!(f, "+")?;
            for _ in 0..self.size {
                write!(f, "--+")?;
            }
            write!(f, "\t+")?;
            for _ in 0..self.size {
                write!(f, "-+")?;
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}

impl FromStr for Taquin {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let map: Vec<u8> = s.split_whitespace().flat_map(|e| e.parse()).collect();

        Taquin::new(map).ok_or(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_grid() {
        assert!(Taquin::new(vec![1; 9]).is_none())
    }

    #[test]
    fn test_solved() {
        let t = Taquin::new(vec![1, 2, 3, 4, 5, 6, 7, 8, 0]).unwrap();
        assert!(t.is_solved());
        assert_eq!(t.dist_to_solved(), 0);

        let t = Taquin::new(vec![1, 2, 3, 4, 5, 6, 7, 0, 8]).unwrap();
        assert!(!t.is_solved());
        assert_ne!(t.dist_to_solved(), 0);
    }
}
