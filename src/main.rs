use std::convert::TryInto;

use pathfinding::prelude::astar;
use taquin::Taquin;

fn main() {
    let mut line = String::new();
    std::io::stdin().read_line(&mut line).unwrap();
    let t: Taquin = line.parse().unwrap();

    println!("START\n{}", t);
    let now = std::time::Instant::now();

    let r = astar(
        &t,
        |state| {
            state
                .actions()
                .map(|action| {
                    let mut s = state.clone();
                    let reward = s.action(action);
                    let reward: usize = (-reward).try_into().unwrap();

                    (s, reward)
                })
                .collect::<Vec<(Taquin, usize)>>()
        },
        Taquin::dist_to_solved,
        Taquin::is_solved,
    );
    let result = r.unwrap().0;
    println!("Solving took {:?}", now.elapsed());
    println!("Step {:?}", result.len());

    for r in result {
        println!("{}", r);
        std::io::stdin().read_line(&mut String::new()).unwrap();
    }
}
