use std::cmp::Ordering;

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Pos {
    pub len: usize,
    pub col: usize,
    pub row: usize,
}

impl Pos {
    pub fn new<T, U>(val: T, len: U) -> Self
    where
        T: Into<usize>,
        U: Into<usize>,
    {
        let val: usize = val.into();
        let len: usize = len.into();
        Pos {
            len,
            col: val % len,
            row: val / len,
        }
    }

    pub fn val(&self) -> usize {
        self.col + self.row * self.len
    }

    pub fn position_to(self, to: Pos) -> Option<PosIter> {
        let drow = self.row.cmp(&to.row);
        let dcol = self.col.cmp(&to.col);

        let delta_pos = match (drow, dcol) {
            (Ordering::Equal, Ordering::Less) => Some(Dir::ColP),
            (Ordering::Equal, Ordering::Greater) => Some(Dir::ColN),

            (Ordering::Less, Ordering::Equal) => Some(Dir::RowP),
            (Ordering::Greater, Ordering::Equal) => Some(Dir::RowN),

            _ => None,
        };

        delta_pos.map(|delta_pos| PosIter {
            next_val: self,
            target_pos: to,
            delta_pos,
        })
    }

    pub fn dist(&self, to: &Pos) -> usize {
        abs_dist(self.row, to.row) + abs_dist(self.col, to.col)
    }
}

fn abs_dist(a: usize, b: usize) -> usize {
    if a > b {
        a - b
    } else {
        b - a
    }
}

#[derive(Debug, PartialEq)]
enum Dir {
    Done,
    ColP,
    ColN,
    RowP,
    RowN,
}

pub struct PosIter {
    next_val: Pos,
    delta_pos: Dir,
    target_pos: Pos,
}

impl Iterator for PosIter {
    type Item = Pos;

    fn next(&mut self) -> Option<Self::Item> {
        let ret = self.next_val.clone();

        if ret == self.target_pos && self.delta_pos != Dir::Done {
            self.delta_pos = Dir::Done;
        } else {
            match self.delta_pos {
                Dir::ColP => self.next_val.col += 1,
                Dir::ColN => self.next_val.col -= 1,
                Dir::RowP => self.next_val.row += 1,
                Dir::RowN => self.next_val.row -= 1,
                Dir::Done => return None,
            }
        }

        Some(ret)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_convert_keep_value() {
        for size in 3..=5 {
            for i in 0..(size * size) {
                let p = Pos::new(i, size);
                let i2 = p.val();

                assert_eq!(i2, i)
            }
        }
    }
}
